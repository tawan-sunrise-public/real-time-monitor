<?php
  $potocal = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '');
  $directory = '/real-time-monitor/';
  $base_url = $potocal . '://' . $_SERVER['HTTP_HOST'] . $directory;

  $timeloop = (!empty($_GET["timeloop"])) ? $_GET["timeloop"] : 5000;

  $pm2_5_outside = (!empty($_GET["pm2_5_outside"])) ? $_GET["pm2_5_outside"] : "-";
  $pm2_5_inside = (!empty($_GET["pm2_5_inside"])) ? $_GET["pm2_5_inside"] : "-";

  $temperature_outside = (!empty($_GET["temperature_outside"])) ? $_GET["temperature_outside"] : "-";
  $temperature_inside = (!empty($_GET["temperature_inside"])) ? $_GET["temperature_inside"] : "-";

  $humidity_outside = (!empty($_GET["humidity_outside"])) ? $_GET["humidity_outside"] : "-";
  $humidity_inside = (!empty($_GET["humidity_inside"])) ? $_GET["humidity_inside"] : "-";
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="favicon.png">

  <title>Realtime Monitor</title>

  <!-- Bootstrap core CSS -->
  <link href="<?=$base_url;?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Slick -->
  <link rel="stylesheet" type="text/css" href="<?=$base_url;?>slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="<?=$base_url;?>slick/slick-theme.css"/>

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Codystar&display=swap" rel="stylesheet">

  <!-- Custom CSS -->
  <style type="text/css">
    body {
    	background-color: black;
    	color: white;
    }
    .heading {
    	height: 20vh;
    	font-size: 10vh;
    	font-family: 'Roboto', sans-serif;
    	font-weight: bold;
    }
	.outside {
		height: 10vh;
    	font-size: 7vh;
		font-family: 'Roboto', sans-serif;
	}
	.inside {
		height: 30vh;
    	font-size: 20vh;
		font-family: 'Roboto', sans-serif;
	}
  </style>

</head>

<body>

  <!-- Page Content -->
  <div class="container">
    <div class="slide">
      <!-- Slide 1 -->
      <div>
        <div class="row align-items-center">
          <div class="col-12 text-center">
            <div class="heading">
              Realtime&nbsp;&nbsp;PM 2.5
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="row align-items-end">
          <div class="col-6 text-center" style="margin-bottom: 6vh;">
            <div class="outside">
              <?=$pm2_5_outside;?>
            </div>
            <div class="outside">
              Outside
            </div>
          </div>
		      <div class="col-6 text-center">
		        <div class="inside" style="font-family: 'Codystar', cursive; font-size: 28vh;-webkit-text-stroke: 0.6vh #fff;">
		          <?=$pm2_5_inside;?>
		        </div>
		        <div class="inside">
		          Inside
		        </div>
		      </div>
	      </div>
      </div>
      <!-- // Slide 1 -->

      <!-- Slide 2 -->
      <div>
        <div class="row">
          <div class="col-12 text-center">
            <div class="heading">
              Realtime&nbsp;&nbsp;Temperature
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <div class="outside">
              <?=$temperature_outside;?>
            </div>
            <div class="outside">
              Outside
            </div>
          </div>
        </div>
	    <div class="row">
		    <div class="col-12 text-center">
		        <div class="inside" style="font-family: 'Codystar', cursive; font-size: 28vh;-webkit-text-stroke: 0.6vh #fff;">
		          <?=$temperature_inside;?>
		        </div>
		        <div class="inside">
		          Inside
		        </div>
		    </div>
	    </div>
      </div>
      <!-- // Slide 2 -->

      <!-- Slide 3 -->
      <div>
        <div class="row">
          <div class="col-12 text-center">
            <div class="heading">
              Realtime&nbsp;&nbsp;Humidity
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <div class="outside">
              <?=$humidity_outside;?>
            </div>
            <div class="outside">
              Outside
            </div>
          </div>
        </div>
	    <div class="row">
		    <div class="col-12 text-center">
		        <div class="inside" style="font-family: 'Codystar', cursive; font-size: 28vh;-webkit-text-stroke: 0.6vh #fff;">
		          <?=$humidity_inside;?>
		        </div>
		        <div class="inside">
		          Inside
		        </div>
		    </div>
	    </div>
      </div>
      <!-- // Slide 3 -->
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="<?=$base_url;?>vendor/jquery/jquery.slim.min.js"></script>
  <script src="<?=$base_url;?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript" src="<?=$base_url;?>jquery-2.2.0.min.js"></script>
  <script type="text/javascript" src="<?=$base_url;?>slick/slick.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('.slide').slick({
        dots: false,
        autoplay: true,
        infinite: true,
        autoplaySpeed: <?=$timeloop;?>,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        arrows: false,
        focusOnSelect: false,
        pauseOnHover:false
      });
    });
  </script>
</body>
</html>
